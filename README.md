# README #

From the help of the matlab file:
**LOADENCASFILE** Loads case/ encas file and puts all variables found in the
encas file in a single tree-like structure.

USAGE: (Loading files)

```
#!matlab

[ encas_file_struct ] = loadEncasFile( filelocation )
[ encas_file_struct ] = loadEncasFile; % asks for encas/ case file.

```


EXAMPLES: (To acces the data)

```
#!matlab

           geometry = encas_file_struct.geometry
    var_wss_on_wall = encas_file_struct.wall_shear.surface3.timestep1
 velocity_timesteps = encas_file_struct.x_velocity.timesteps
timing_of_timesteps = encas_file_struct.encas_file.timings

```


Please note that these function might not work with ALL case/encas
files.  (Only tested with selected encas files exported by Ansys Fluent: 
          transient simulation (>1 TIMESTEPS) & (single) static geometry)

Intended as a guideline for loading encas files in matlab; feel free to
edit/ improve / add features/ ask questions.

## VISUALISATION EXAMPLE ##
show triangles from surface in red

```
#!matlab

f1 = figure(1); set(f1,'color','w'); cla; % make new figure window; empty axes
patch('vertices',encas_contents.geometry.wall.vertices,... % select wall vertices
      'faces',encas_contents.geometry.wall.faces_tria3,... % select wall triangle faces
      'facecolor','r',... % red color of triangle
      'edgecolor','k');  % with black edge color
view(3); axis equal vis3d off; rotate3d on % show it in a 3d fashion.
 
show triangles from surface with WSS colors

```

```
#!matlab

f2 = figure(2); set(f2,'color','w'); cla; % make new figure window; empty axes
patch('vertices',encas_contents.geometry.wall.vertices,... % select wall vertices
      'faces',encas_contents.geometry.wall.faces_tria3,... % select wall triangle faces
      'facecolor','interp',... % interp face color from vertices
      'cdata',(encas_contents.wall_shear.wall.timestep1),... %select ColorData as double datatype from wal_shear timestep1
      'edgecolor','k');  % with black edge color
colorbar;
view(3); axis equal vis3d off; rotate3d on % show it in a 3d fashion.
 

```
show QUAD ELEMENT from INT_BLOOD

```
#!matlab

f3 = figure(3); set(f3,'color','w'); cla; % make new figure window; empty axes
patch('vertices',encas_contents.geometry.int_blood.vertices,... % select wall vertices
      'faces',encas_contents.geometry.int_blood.faces_quad4,... % select wall QUAD4 faces
      'facecolor','r',... % red color
      'facealpha',0.5,... % with transparency 50%
      'edgecolor','k');  % with black edge color
view(3); axis equal vis3d off; rotate3d on % show it in a 3d fashion.
 
hold on; % add another patch on top of it
patch('vertices',encas_contents.geometry.int_blood.vertices,... % select wall vertices
      'faces',encas_contents.geometry.int_blood.faces_tria3,... % select wall TRIANGLE faces
      'facecolor','b',... % blue color
      'facealpha',0.3,... % with transparency 30%
      'edgecolor','k');   % with black edge color
```